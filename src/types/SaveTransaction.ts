export default interface SaveTransaction {

    _id?: number;

    timestamp?: Date;
  
    sender: string; // User ID

    senderRemain?: number;
  
    receiver: string; // User ID

    receiverId? : string; // Account id of receiver
  
    remain: number;
  
    amount: number;

    type: string; // 'Deposit', 'Withdraw', 'Transfer'

    side: string;
}