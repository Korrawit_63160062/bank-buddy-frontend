export default interface Transaction {

    _id?: number;

    username: string;

    password?: string;
    
    accounts?: string[]; // Array of Account IDs

    transactions?: Transaction[]; // Array of Transaction objects
}
