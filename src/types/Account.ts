export default interface Account {

    _id?: number;

    balance: number;

    owner?: string;

    username? : string;
}