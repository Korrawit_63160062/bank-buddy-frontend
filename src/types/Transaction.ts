import type User from "./User";

export default interface Transaction {

    _id?: number;

    timestamp?: Date;
  
    sender: User; 
  
    receiver: User; 
  
    remain: number;
  
    amount: number;

    type?: string; // 'Deposit', 'Withdraw', 'Transfer'
    side? : string;
}