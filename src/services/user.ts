import http from './axios'

function getById(id: string) {
    return http.get(`/users/${id}`);
}



export default { getById }