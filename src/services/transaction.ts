import http from './axios'
import type SaveTransaction from '@/types/SaveTransaction';

function getTransactions() {
    return http.get('/transactions');
}

function getBySenderId(senderId: string) {
  return http.get(`/transactions/sender/${senderId}`);
}

function findTransactionsByUser(userId : string) {
  return http.get(`/transactions/transactions/user/${userId}`);
}
function saveTransaction(saveTransaction: SaveTransaction) {
    return http.post("/transactions", saveTransaction);
  }

export default { getTransactions, saveTransaction, getBySenderId, findTransactionsByUser }