import type Account from '@/types/Account';
import http from './axios'

function getByOwnerId(ownerId: string) {
    return http.get(`/accounts/owner/${ownerId}`);
}

function getById(id: string) {
    return http.get(`/accounts/${id}`);
}


function updateAccount(id: string, account: Account) {
    return http.patch(`/accounts/${id}`, account);
}


export default { getByOwnerId, updateAccount, getById }