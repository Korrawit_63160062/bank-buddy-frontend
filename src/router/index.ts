import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      components: {
        default: HomeView,
        menu: () => import('@/components/menus/MainMenu.vue'),
        header: () => import('@/components/headers/MainHeader.vue'),
      },
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when sdthe route is visited.
      component: () => import('../views/AboutView.vue'),
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/transactionhistory',
      name: 'transactionhistory',
      components: {
        default: () => import('../views/transactions/TransactionHistoryView.vue'),
        menu: () => import('@/components/menus/MainMenu.vue'),
        header: () => import('@/components/headers/MainHeader.vue'),
      },
    },
    {
      path: '/transaction',
      name: 'transaction',
      components: {
        default: () => import('../views/transactions/TransactionView.vue'),
        menu: () => import('@/components/menus/MainMenu.vue'),
        header: () => import('@/components/headers/MainHeader.vue'),
      },
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/login",
      name: "login",
      components: {
        default: () => import('../views/login/LoginView.vue'),
      },
      meta: {
        requiresAuth: true
      }
    },
  ]
});

export default router;
