import { ref } from 'vue'
import { defineStore } from 'pinia'
import type Transaction from '@/types/Transaction';
import transactionService from '@/services/transaction';
import accountService from '@/services/account';
import type SaveTransaction from '@/types/SaveTransaction';
import type Account from '@/types/Account';
import { useAccountStore } from './account';
import { useLoadingStore } from './loading';
import { useMessageStore } from './message';
import { useAuthStore } from './auth';
import router from '@/router';

export const useTransactionStore = defineStore('transaction', () => {
  const accountStore = useAccountStore();
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const authStore = useAuthStore();
  const userAccountId = ref('');
  const transactions = ref<Transaction[]>([]);
  const userAccount = ref<Account>();  // For Deposit, Withdraw
  const userAccountTransferTo = ref<Account>();  // For Transfer
  const receiverAccountId = ref('');
  const receiverUserId = ref('');
  const currentUserBalance = ref(0);

  const editedSaveTransaction = ref<SaveTransaction>({  // For Deposit, Withdraw
    timestamp: undefined,
    sender: "",
    senderRemain: 0,
    receiver: "",
    receiverId: "",
    remain: 0,
    amount: 0,
    type: 'Transfer',
    side: 'Sender'
  });

  const editedSaveTransactionReceiver = ref<SaveTransaction>({ // For Transfer
    timestamp: undefined,
    sender: "",
    senderRemain: 0,
    receiver: "",
    receiverId: "",
    remain: 0,
    amount: 0,
    type: 'Transfer',
    side: 'Receiver'
  });

  async function getTransactions() {
    try {
      const res = await transactionService.getTransactions();
      transactions.value = res.data;
    } catch (e) {
      console.log(e)
    }
  }

  async function getTransactionsBySenderId(senderId: string) {

    try {
      const userData = localStorage.getItem("user");
      if (userData) {
        const tempUser = JSON.parse(userData);
        const res = await transactionService.getBySenderId(tempUser._id);
        transactions.value = res.data;
      } else {
        const res = await transactionService.getBySenderId(senderId);
        transactions.value = res.data;
      }
    } catch (e) {
      console.log(e)
    }
  }

  async function getCurrentUserBalance() {
    const userData = localStorage.getItem("user");
    if (userData) {
      const tempUser = JSON.parse(userData);
      await getAccountByOwnerId(tempUser._id);
      currentUserBalance.value = userAccount.value!.balance;
    } else {
      await getAccountByOwnerId(authStore.userId);
      currentUserBalance.value = userAccount.value!.balance;
    }
}

  async function getTransactionsByUserId(userId: string) {

    try {
      const userData = localStorage.getItem("user");
      if (userData) {
        const tempUser = JSON.parse(userData);
        const res = await transactionService.findTransactionsByUser(tempUser._id);
        transactions.value = res.data;
      } else {
        const res = await transactionService.findTransactionsByUser(userId);
        transactions.value = res.data;
      }
    } catch (e) {
      console.log(e)
    }
  }

  async function getAccountByOwnerId(ownerId: string) {
    try {
      const res = await accountService.getByOwnerId(ownerId);
      userAccount.value = res.data[0];
    } catch (e) {
      console.log(e)
    }
  }

  async function getReceiverAccountByAccountId() {
    try {
      const res = await accountService.getById(receiverAccountId.value);
      userAccountTransferTo.value = res.data;

      receiverUserId.value = res.data.owner;
      editedSaveTransaction.value.receiver = receiverUserId.value;
      
    } catch (e) {
      console.log(e)
    }
  }

  async function saveTransaction() {
    console.log(editedSaveTransactionReceiver.value.side);
    editedSaveTransaction.value.timestamp = new Date(new Date().toISOString());
    const userData = localStorage.getItem("user");
    if (userData) {
      const tempUser = JSON.parse(userData);
      editedSaveTransaction.value.sender = tempUser._id;
      editedSaveTransaction.value.receiver = tempUser._id;
    } else if (authStore.userId) {
      editedSaveTransaction.value.sender = authStore.userId;
      editedSaveTransaction.value.receiver = authStore.userId;
    } else {
      router.replace("/login")
    }

    if (editedSaveTransaction.value.type == 'Deposit') {   // For Deposit
      editedSaveTransaction.value.remain =
        Number(userAccount.value!.balance) + Number(editedSaveTransaction.value.amount);
    }
    else if (editedSaveTransaction.value.type == 'Withdraw') {   // For WithDraw
      editedSaveTransaction.value.remain =
        Number(userAccount.value!.balance) - Number(editedSaveTransaction.value.amount);
    }
    else if (editedSaveTransaction.value.type == 'Transfer') {   // For Transfer
      await getReceiverAccountByAccountId();
        const senderRemain = Number(userAccount.value!.balance) - Number(editedSaveTransaction.value.amount);
        const receiverRemain = Number(userAccountTransferTo.value!.balance) + Number(editedSaveTransaction.value.amount);
        
        console.log(editedSaveTransactionReceiver.value.side);
        editedSaveTransaction.value.remain = senderRemain;
        editedSaveTransactionReceiver.value = { ...editedSaveTransaction.value, remain: receiverRemain, side: 'Receiver' };
        editedSaveTransactionReceiver.value.receiverId = receiverUserId.value;
        editedSaveTransactionReceiver.value.senderRemain = senderRemain;
        console.log(editedSaveTransactionReceiver.value.side);
                
      }
      

    loadingStore.showWithMessage("Transaction in Progress");
    if (editedSaveTransaction.value.type != 'Transfer') {
      editedSaveTransaction.value.side = 'User';
      try {
        accountStore.editedAccount.balance = editedSaveTransaction.value.remain;
        accountStore.saveAccount(userAccount.value!._id!.toString());
        await transactionService.saveTransaction(editedSaveTransaction.value);
        messageStore.showMessage("Transaction is Completed", 5000);
      }
      catch (exception) {
        loadingStore.closeDialog(0);
        messageStore.showError(
          "Request Failed!",
          exception.response.data.message,
          exception.response.status
        );
        console.log(exception);
      }
      loadingStore.closeDialog(0);
    } else {
      try {
        accountStore.editedAccount.balance = editedSaveTransaction.value.remain;
        await accountStore.saveAccount(userAccount.value!._id!.toString());

        accountStore.editedAccount.balance = editedSaveTransactionReceiver.value.remain;
        await accountStore.saveAccount(userAccountTransferTo.value!._id!.toString());

        await transactionService.saveTransaction(editedSaveTransactionReceiver.value);

        messageStore.showInfo("Transaction is Completed");
      }
      catch (exception) {
        loadingStore.closeDialog(0);
        messageStore.showError(
          "Request Failed!",
          exception.response.data.message,
          exception.response.status
        );
        console.log(exception);
      }
      loadingStore.closeDialog(0)
    }
    reset();
    accountStore.resetTempAccount();
    await getTransactions();
  }

  function reset() {
    receiverAccountId.value = '';
    editedSaveTransaction.value = {
      timestamp: undefined,
      sender: "",
      receiver: "",
      remain: 0,
      amount: 0,
      type: 'Transfer',
      side: 'Sender'
    };
    editedSaveTransactionReceiver.value = {
      timestamp: undefined,
      sender: "",
      receiver: "",
      receiverId: "",
      remain: 0,
      amount: 0,
      type: 'Transfer',
      side: 'Receiver'
    };
  }

  return { getTransactionsByUserId, transactions, getTransactions, getTransactionsBySenderId, editedSaveTransaction, saveTransaction, userAccountId, getAccountByOwnerId, userAccount, receiverAccountId, getCurrentUserBalance,currentUserBalance }
})
