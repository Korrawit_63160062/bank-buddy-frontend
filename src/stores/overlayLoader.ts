import { ref } from "vue";
import { defineStore } from "pinia";

export const useOverlayLoaderStore = defineStore("overlayLoader", () => {
  const overlayLoader = ref(false);

  return { overlayLoader };
});
