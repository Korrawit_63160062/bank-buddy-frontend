import { ref } from "vue";
import { defineStore } from "pinia";

export const useLoadingStore = defineStore("loading", () => {
  const isLoading = ref(false);
  const showExtendedMessage = ref(false);
  const extendedMessage = ref("");
  const message = ref("");
  const barColour = ref("primary");

  const showWithMessage = (msg: string) => {
    isLoading.value = true;
    message.value = msg;
  };

  const showWarningMessage = (msg?: string, extMsg?: string) => {
    if (msg) {
      message.value = msg;
    }
    if (extMsg) {
      extendedMessage.value = extMsg;
    }
    showExtendedMessage.value = true;
    barColour.value = "red";
  };

  const closeDialog = (ms: number) => {
    setTimeout(function () {
      isLoading.value = false;
      setTimeout(function () {
        showExtendedMessage.value = false;
        message.value = "";
        extendedMessage.value = "";
        barColour.value = "primary";
      }, 200);
    }, ms);
  };

  return {
    isLoading,
    showWithMessage,
    showExtendedMessage,
    showWarningMessage,
    message,
    extendedMessage,
    barColour,
    closeDialog,
  };
});
