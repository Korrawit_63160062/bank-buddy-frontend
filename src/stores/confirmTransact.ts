import { defineStore } from "pinia";
import { ref } from "vue";

export const useConfirmTransact = defineStore("confirmTransact", () => {

    const dialog = ref(false);
    const notifications = ref(false);
    const sound = ref(false);
    const widgets = ref(false);

  return { dialog, notifications, sound, widgets };
});
