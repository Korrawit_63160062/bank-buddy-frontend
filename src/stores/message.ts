import { ref } from "vue";
import { defineStore } from "pinia";

export const useMessageStore = defineStore("message", () => {
  const isShow = ref(false);
  const message = ref("");
  const eMessage = ref("");
  const timeout = ref(2000);
  const type = ref(0);
  const statusCode = ref(0);

  const showMessage = (msg: string, tout = 2000) => {
    message.value = msg;
    isShow.value = true;
    timeout.value = tout;
  };

  const closeMessage = () => {
    message.value = "";
    eMessage.value = '';
    statusCode.value = 0;
    isShow.value = false;
  };

  const showError = (text: string, eText: string, sCode?: number) => {
    if (sCode) {
      statusCode.value = sCode;
    }
    type.value = 1;
    message.value = text;
    eMessage.value = eText;
    isShow.value = true;
  };

  const showInfo = (text: string) => {
    type.value = 0;
    message.value = text;
    isShow.value = true;
  };

  const showConfirm = (text: string) => {
    type.value = 2;
    message.value = text;
    isShow.value = true;
  };

  return {
    isShow,
    message,
    eMessage,
    statusCode,
    showMessage,
    closeMessage,
    timeout,
    showError,
    showInfo,
    showConfirm,
  };
});
