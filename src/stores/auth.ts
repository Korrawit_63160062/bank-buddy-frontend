import { defineStore } from "pinia";
import auth from "@/services/auth";
import router from "@/router";
import { useMessageStore } from "./message";
import { useLoadingStore } from "./loading";
import { ref } from "vue";
import { useTransactionStore } from "./transaction";

export const useAuthStore = defineStore("auth", () => {

  const messageStore = useMessageStore();
  const transactionStore = useTransactionStore();
  const loadingStore = useLoadingStore();
  const userId = ref('');

  const isLogin = () => {
    const user = localStorage.getItem("user");
    return user ? true : false;
  };

  const getLocalUser = () => {
    const userData = localStorage.getItem("user");
    if (userData) {
      const tempUser = JSON.parse(userData);
      userId.value = tempUser._id;
  }
  }
  const login = async (userName: string, password: string): Promise<void> => {
    loadingStore.isLoading = true;
    try {
      const res = await auth.login(userName, password);
      userId.value = res.data.user._id;
      localStorage.setItem("user", JSON.stringify(res.data.user));
      localStorage.setItem("token", res.data.access_token);
      transactionStore.getAccountByOwnerId(userId.value);
      router.push("/");
    } catch (e) {
      console.log(e)
      messageStore.showError("Username หรือ Password ไม่ถูกต้อง");
    }
    loadingStore.isLoading = false;
  };

  const logout = () => {
    localStorage.removeItem("user");
    localStorage.removeItem("token");
    router.replace("/login");
  };

  return { login, logout, isLogin, userId, getLocalUser };
});
