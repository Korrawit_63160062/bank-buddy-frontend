import { ref } from 'vue'
import { defineStore } from 'pinia'
import accountService from '@/services/account';
import userService from '@/services/user';
import type Account from '@/types/Account';
import { useTransactionStore } from './transaction';
import { useAuthStore } from './auth';

export const useAccountStore = defineStore('account', () => {
    const transactionStore = useTransactionStore();
    const authStore = useAuthStore();
    const editedAccount = ref<Account>({
        balance: 0,
    });

    const tempTransferAccount = ref<Account>({
        balance: 0,
        username: ''
    });

    async function saveAccount(id: string) {
        try {
            await accountService.updateAccount(id, editedAccount.value);
            const userData = localStorage.getItem("user");
            if (userData) {
                const tempUser = JSON.parse(userData);
                transactionStore.getAccountByOwnerId(tempUser._id);
            }
            resetEditedAccount();
        } catch (exception) {
            console.log(exception);

        }
    }

    async function checkAccountTransferValid(id: string) { // Account is exist, Not the sender account
        try {
            const res = await accountService.getById(id);
            if (res.data) {
                if (res.data.owner == authStore.userId) {
                    return 'repeat';
                } else {
                    const resUser = await userService.getById(res.data.owner);                    
                    tempTransferAccount.value = res.data;
                    tempTransferAccount.value.username = resUser.data.username;
                    return 'valid';
                }
            } {
                return 'invalid';
            }
        } catch (exception) {
            console.log(exception);
        }
    }

    function resetEditedAccount() {
        editedAccount.value = {
            balance: 0,
        };
    }

    function resetTempAccount() {
        tempTransferAccount.value = {
            balance: 0,
            username: ''
        };
    }

    return { editedAccount, saveAccount, checkAccountTransferValid, tempTransferAccount, resetTempAccount }
})
